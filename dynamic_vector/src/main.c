#include <stdio.h>
#include <stdlib.h>

#include "vector.c"

int main(void) {
  printf("=== output vector in C ===\n\n");

  vector vec;
  v(&vec);
  v_add(&vec, "t12222222222222222222");
  v_add(&vec, "t2222222222222222222222222222222222");

  printf("%ld\n", sizeof(vec));
  printf("%s\n ", (char *) v_get(&vec, 1));

  v_set(&vec, 1, "tata");
  printf("%s\n ", (char *) v_get(&vec, 1));


  return 0;
}