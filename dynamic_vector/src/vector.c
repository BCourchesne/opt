#include <stdio.h>
#include <stdlib.h>

#include "vector.h"

void v(vector *vec) {
  vec->cap =  sizeof(void *);
  vec->count = 0;
  vec->elements = malloc(sizeof(void *) * vec->cap);
}

static void v_rezise(vector *vec, int cap) {
  void **elements = realloc(vec->elements, sizeof(void *) * cap);

  if (elements) {
    vec->elements = elements;
    vec->cap = cap;
  }
}

void v_add(vector *vec, void *element) {
  if(vec->cap == vec->count) {
    v_rezise(vec, vec->cap * 2);
  }
  
  vec->elements[vec->count++] = element;
}

void v_set(vector *vec, int index, void *element) {
  if(index >= 0 && index < vec->count) {
    vec->elements[index] = element;
  }
}

void *v_get(vector *vec, int index) {
  if (index >= 0 && index < vec->count) {
    return vec->elements[index];
  }
  return NULL;
}

void v_del(vector *vec, int index) {
  if ( index < 0 && index >= vec ->count) {
    return
  }

  for ()
}

