#ifndef VECTOR_H
#define VECTOR_H

typedef struct vector {
  void **elements;
  int count;
  int cap;
} vector;

void v(vector *);
void v_add(vector *, void *);
void v_set(vector *, int, void *);
void *v_get(vector *, int);
void v_del(vector *, int);
void v_free(vector *);

static void v_resize(vector *, int);

#endif